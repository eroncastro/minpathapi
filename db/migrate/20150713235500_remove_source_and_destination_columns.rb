class RemoveSourceAndDestinationColumns < ActiveRecord::Migration
  def change
    remove_column :routes, :source
    remove_column :routes, :destination
  end
end
class RenameFuelColumnAsDistance < ActiveRecord::Migration
  def change
    rename_column :routes, :fuel, :distance
  end
end
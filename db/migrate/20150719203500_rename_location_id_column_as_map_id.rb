class RenameLocationIdColumnAsMapId < ActiveRecord::Migration
  def change
    rename_column :routes, :location_id, :map_id
  end
end

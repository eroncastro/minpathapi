class RenameLocationTableAsMapTable < ActiveRecord::Migration
  def change
    rename_table :locations, :maps
  end
end

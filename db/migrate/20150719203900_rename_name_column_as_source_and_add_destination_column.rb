class RenameNameColumnAsSourceAndAddDestinationColumn < ActiveRecord::Migration
  def change
    rename_column :routes, :name, :source
    add_column :routes, :destination, :string
  end
end

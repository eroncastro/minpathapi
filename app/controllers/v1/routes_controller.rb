class V1::RoutesController < ApplicationController
  before_action :validate_content_type, only: :create
  before_action :validate_create_params, only: :create
  before_action :validate_calculator_params, only: :index
  skip_before_action :verify_authenticity_token

  def index
    @optimum_path = Routes::OptimumPathFinder.new(params).find_optimum_path
    @map_name = params[:map_name]
    @source = params[:source]
    @destination = params[:destination]

    render 'v1/routes/index.json.jbuilder', status: 200, format: :json
  end

  def create
    Routes::Creator.new(params).build_new_map
    render json: { status: t(:created_with_success, name: params[:name]) }, status: 201
  end

  private

  def validate_content_type
    content_type_validator = Routes::ContentTypeValidator.new(
      request.headers['content-type'])

    if content_type_validator.invalid?
      render json: { error: content_type_validator.errors.messages },
        status: 415 and return
    end
  end

  def validate_calculator_params
    params_validator = Routes::CalculatorParamsValidator.new(params)
    if params_validator.invalid?
      render json: { errors: params_validator.errors.messages }, status: 400 and return
    end
  end

  def validate_create_params
    params_validator = Routes::ParamsValidator.new(params)
    if params_validator.invalid?
      render json: { errors: params_validator.errors.messages }, status: 400 and return
    end
  end

  def t(param, args = {})
    I18n.t("controllers.v1.routes.#{param}", args)
  end

  def route_fields
    %w(source destination distance)
  end
end

module Routes
  class Creator
    def initialize(params)
      @name = params[:name]
      @routes = params[:routes]
    end

    def build_new_map
      ActiveRecord::Base.transaction do
        map = Map.find_or_create_by!(name: @name)
        @routes.each do |source, destination, distance|
          Route.find_or_create_by!(
            source: source,
            destination: destination,
            map_id: map.id,
            distance: distance
          )
        end
      end
    end
  end
end

module Routes
  class Graph
    def initialize(map_name)
      @map_name = map_name
    end

    def nodes
      @nodes ||= generated_graph[:nodes]
    end

    def edges
      @edges ||= generated_graph[:edges]
    end

    private

    def generated_graph
      @generated_graph ||= begin
        graph = {
          nodes: [],
          edges: {}
        }
        map = Map.find_by(name: @map_name)
        return graph if map.nil?

        routes = map.routes.pluck(:source, :destination, :distance)

        routes.each do |source, destination, distance|
          graph[:nodes] << [source, destination]
          graph[:edges][source] = [] if graph[:edges][source].nil?
          graph[:edges][source] << [destination, distance]
        end
        graph[:nodes] = graph[:nodes].flatten.uniq
        graph
      end
    end
  end
end

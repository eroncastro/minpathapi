module Routes
  class CalculatorParamsValidator
    include ActiveModel::Validations

    attr_accessor :map_name, :source, :destination, :autonomy, :fuel_liter_price

    validates_presence_of(
      :map_name, :source, :destination, :autonomy, :fuel_liter_price
    )
    validate :map_exists
    validate :source_and_destination_exist
    validate :autonomy_contains_a_real_number_greater_than_zero
    validate :fuel_liter_price_contains_a_real_number_greater_than_zero

    def initialize(params)
      @map_name = params[:map_name]
      @source = params[:source]
      @destination = params[:destination]
      @autonomy = params[:autonomy]
      @fuel_liter_price = params[:fuel_liter_price]
    end

    private

    [:autonomy, :fuel_liter_price].each do |param|
      define_method("#{param}_contains_a_real_number_greater_than_zero") do
        return if !send(param).nil? && send(param).to_f > 0

        errors.add(param.to_sym, :must_be_a_real_number_greater_than_zero)
      end
    end

    def map_exists
      return if map_name.blank? || Map.find_by(name: map_name)

      errors.add(:map_name, :not_found_error, map_name: map_name)
    end

    def source_and_destination_exist
      [:source, :destination].each do |field|
        unless Route.find_by("#{field}": send(field))
          errors.add(field, :not_found_error, "#{field}": send(field))
        end
      end
    end
  end
end

module Routes
  class ContentTypeValidator
    include ActiveModel::Validations

    attr_accessor :content_type

    validate :content_type_is_valid

    def initialize(content_type)
      @content_type = content_type
    end

    private

    def content_type_is_valid
      return if valid_content_type.include?(@content_type)

      errors.add(:content_type, :content_type_is_not_valid,
        supplied_content_type: @content_type,
        valid_content_type: valid_content_type.first
      )
    end

    def valid_content_type
      %w(application/json)
    end
  end
end
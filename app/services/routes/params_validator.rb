class Routes::ParamsValidator
  include ActiveModel::Validations

  attr_accessor(:name, :routes)

  validates_presence_of :name
  validate :routes_param_is_a_non_empty_array
  validate :routes_elements

  def initialize(params)
    @name = params[:name]
    @routes = params[:routes]
  end

  private

  def routes_param_is_a_non_empty_array
    return if routes_param_is_not_a_non_empty_array?

    errors.add(:routes, :routes_param_must_be_a_non_empty_array)
  end

  def routes_param_is_not_a_non_empty_array?
    !routes.nil? && routes.is_a?(Array) && !routes.flatten.empty?
  end

  def routes_elements
    return unless routes_param_is_not_a_non_empty_array?

    routes.each do |route|
      verify_route_has_the_correct_format(route)
      if route_has_the_correct_format?(route)
        verify_source_is_diffent_from_destination(route)
        verify_distance_is_a_number_greater_than_zero(route)
      end
    end

    verify_there_are_no_duplicated_routes
  end

  def verify_route_has_the_correct_format(route)
    return if route_has_the_correct_format?(route)

    errors.add(:routes, :route_format_error, route: route)
  end

  def route_has_the_correct_format?(route)
    route.any? && route.size == 3
  end

  def verify_source_is_diffent_from_destination(route)
    return if route[0] != route[1]

    errors.add(:routes, :source_and_destination_are_equal,
      route: route)
  end

  def verify_distance_is_a_number_greater_than_zero(route)
    return if route[2].to_f > 0.0

    errors.add(:routes, :distance_must_be_a_real_number_greater_than_zero,
      route: route)
  end

  def verify_there_are_no_duplicated_routes
    return if duplicated_routes.empty?

    errors.add(:duplicated_routes, :duplicated_routes, duplicated_routes: duplicated_routes)
  end

  def duplicated_routes
    @duplicates_routes ||= begin
      routes.group_by{ |source, destination, distance| [source, destination]}
        .select{ |k, v| v.size > 1 }
        .values
    end
  end
end

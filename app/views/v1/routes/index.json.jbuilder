json.map_name @map_name
json.source @source
json.destination @destination

json.best_route do
  json.nodes @optimum_path[:nodes]
  json.distance @optimum_path[:distance]
  json.cost @optimum_path[:cost]
end

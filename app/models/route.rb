class Route < ActiveRecord::Base
  belongs_to :location

  validates_presence_of :map_id, :source, :destination, :distance
end
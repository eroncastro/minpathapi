class Map < ActiveRecord::Base
  has_many :routes

  validates_presence_of :name
  validates_uniqueness_of :name
end
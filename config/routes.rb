Rails.application.routes.draw do
  namespace :v1 do
    get '/routes' => 'routes#index'
    post '/routes' => 'routes#create'
  end
end


describe Route do
  describe 'validations' do
    it { should validate_presence_of(:map_id) }
    it { should validate_presence_of(:source) }
    it { should validate_presence_of(:destination) }
    it { should validate_presence_of(:distance) }
  end
end
describe Routes::Creator do
  let(:params) do
    {
      name: name,
      routes: routes
    }
  end

  let(:name) { 'São Paulo' }
  let(:routes) do
    [
      ["A", "B", 10],
      ["B", "D", 15],
      ["A", "C", 20],
      ["C", "D", 30],
      ["B", "E", 50],
      ["D", "E", 30]
    ]
  end

  subject(:route_creator) { described_class.new(params) }
  let(:build_new_map) { route_creator.build_new_map }

  context 'when transaction is successful' do
    it { expect { build_new_map }.to_not raise_error }

    context 'after new route is created' do
      before { build_new_map }

      it { expect(Map.count).to eq 1 }
      it { expect(Route.count).to eq 6 }
      it { expect(Route.find_by(destination: 'B').map_id).to eq Map.find_by(name: name).id }
    end
  end
end

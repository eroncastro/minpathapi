describe Routes::ContentTypeValidator do
  subject(:content_type_validator) { described_class.new(content_type) }

  describe 'validations' do
    context 'when Content-Type is application/json' do
      let(:content_type) { 'application/json' }

      it { is_expected.to be_valid }
    end

    ['invalid', 'text/html', 'text/plain'].each do |content_type|
      context "when Content-Type is #{content_type}" do
        let(:content_type) { content_type }

        it { is_expected.to be_invalid }

        context 'after validation' do
          before { content_type_validator.valid? }

          it 'returns correct error message' do
            expect(content_type_validator.errors.messages[:content_type].first)
              .to include("O formato hipermídia '#{content_type}' fornecido não é valido.")
          end
        end
      end
    end
  end
end
describe Routes::ParamsValidator do
  describe 'validations' do
    let(:params) do
      {
        name: name,
        routes: routes
      }
    end
    let(:name) { 'São Paulo' }
    let(:routes) do
      [
        ["A", "B", 10],
        ["B", "D", 15],
        ["A", "C", 20],
        ["C", "D", 30],
        ["B", "E", 50],
        ["D", "E", 30]
      ]
    end

    subject(:params_validator) { described_class.new(params) }

    context 'when all params are supplied correctly' do
      it { is_expected.to be_valid }
    end

    context 'when name is not supplied' do
      before { params.delete(:name) }

      it { is_expected.to be_invalid }

      context 'after validation' do
        before { params_validator.valid? }
        it 'adds error message to name field' do
          expect(params_validator.errors.messages[:name])
            .to include("O campo nome é obrigatório e não pode estar em branco.")
        end
      end
    end

    [nil, [], 'not_an_array'].each do |routes|
      context "when routes is #{routes}" do
        let(:routes) { routes }

        it { is_expected.to be_invalid }

        context 'after validation' do
          before { params_validator.valid? }

          it 'adds error message to routes param' do
            expect(params_validator.errors.messages[:routes].first)
              .to include("registros estão no formato ['origem', 'destino', distância]")
          end
        end
      end
    end

    context 'when routes contains elements with params missing' do
      let(:routes) do
        [
          ['A', 'B', 10],
          [nil]
        ]
      end

      it { is_expected.to be_invalid }

      context 'after validation' do
        before { params_validator.valid? }

        it 'adds error message to routes elem' do
          expect(params_validator.errors.messages[:routes])
            .to include("A rota '[nil]' não segue o formato ['origem', 'destino', distância].")
        end
      end
    end


    context 'when routes contains elements where source == destination' do
      let(:routes) do
        [
          ['A', 'B', 10],
          ['A', 'A', 1]
        ]
      end

      it { is_expected.to be_invalid }

      context 'after validation' do
        before { params_validator.valid? }

        it 'adds error message to routes elem' do
          expect(params_validator.errors.messages[:routes])
            .to include("Na rota '[\"A\", \"A\", 1] a origem é igual ao destino.")
        end
      end
    end

    ['I am not a number', 0, -1].each do |distance|
      context 'when route has an invalid distance' do
        let(:routes) do
          [
            ['A', 'B', distance]
          ]
        end

        it { is_expected.to be_invalid }

        context 'after validation' do
          before { params_validator.valid? }

          it 'adds error message to distance field' do
            expect(params_validator.errors.messages[:routes].first)
              .to include("Para que a rota #{routes.first.to_s} seja válida")
          end
        end
      end
    end
  end
end

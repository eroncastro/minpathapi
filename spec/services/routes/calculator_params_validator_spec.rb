describe Routes::CalculatorParamsValidator do
  let(:params) do
    {
      map_name: map_name,
      source: source,
      destination: destination,
      autonomy: autonomy,
      fuel_liter_price: fuel_liter_price
    }
  end
  let(:map_name) { name }
  let(:name) { 'SP' }
  let(:source) { 'A' }
  let(:destination) { 'B' }
  let(:autonomy) { '10' }
  let(:fuel_liter_price) { '2.5' }

  let!(:sp) { create(:map, name: name) }

  let!(:a_b) { create(:route, source: 'A', destination: 'B', map_id: sp.id, distance: 10) }
  let!(:b_d) { create(:route, source: 'B', destination: 'D', map_id: sp.id, distance: 15) }
  let!(:a_c) { create(:route, source: 'A', destination: 'C', map_id: sp.id, distance: 20) }
  let!(:c_d) { create(:route, source: 'C', destination: 'D', map_id: sp.id, distance: 30) }
  let!(:b_e) { create(:route, source: 'B', destination: 'E', map_id: sp.id, distance: 50) }
  let!(:d_e) { create(:route, source: 'D', destination: 'E', map_id: sp.id, distance: 30) }

  subject(:calculator_params_validator) { described_class.new(params) }

  context 'when params are supplied correctly' do
    it { is_expected.to be_valid }
  end

  [:map_name, :source, :destination, :autonomy, :fuel_liter_price].each do |param|
    context "when #{param} is not supplied" do
      before { params.delete(param) }

      it { is_expected.to be_invalid }

      context 'after validation' do
        before { calculator_params_validator.valid? }

        it 'adds the correct error message to the field' do
          expect(calculator_params_validator.errors.messages[param])
            .to include("O campo '#{param}' é obrigatório e não pode estar em branco.")
        end
      end
    end
  end

  ['0', 'A', '-1'].each do |autonomy|
    context 'when autonomy is not a real number greater than zero' do
      let(:autonomy) { autonomy }

      it { is_expected.to be_invalid }

      context 'after validation' do
        before { calculator_params_validator.valid? }

        it 'adds the correct error message to the field' do
          expect(calculator_params_validator.errors.messages[:autonomy])
            .to include("O campo 'autonomy' deve conter um número real maior que zero.")
        end
      end
    end
  end

  ['0', 'A', '-1'].each do |fuel_liter_price|
    context 'when fuel_liter_price is not a real number greater than zero' do
      let(:fuel_liter_price) { fuel_liter_price }

      it { is_expected.to be_invalid }

      context 'after validation' do
        before { calculator_params_validator.valid? }

        it 'adds the correct error message to the field' do
          expect(calculator_params_validator.errors.messages[:fuel_liter_price])
            .to include("O campo 'fuel_liter_price' deve conter um número real maior que zero.")
        end
      end
    end
  end

  context 'when map does not exist in DB' do
    let(:map_name) { 'a' }

    it { is_expected.to be_invalid }

    context 'after validation' do
      before { calculator_params_validator.valid? }

      it 'adds the correct error message to the field' do
        expect(calculator_params_validator.errors.messages[:map_name])
          .to include("O mapa de nome '#{map_name}' não foi encontrado.")
      end
    end
  end

  context 'when source does not exist in DB' do
    let(:source) { 'G' }

    it { is_expected.to be_invalid }

    context 'after validation' do
      before { calculator_params_validator.valid? }

      it 'adds the correct error message to the field' do
        expect(calculator_params_validator.errors.messages[:source])
          .to include("A origem '#{source}' não foi encontrada.")
      end
    end
  end

  context 'when destination does not exist in DB' do
    let(:destination) { 'G' }

    it { is_expected.to be_invalid }

    context 'after validation' do
      before { calculator_params_validator.valid? }

      it 'adds the correct error message to the field' do
        expect(calculator_params_validator.errors.messages[:destination])
          .to include("O destino '#{destination}' não foi encontrado.")
      end
    end
  end
end

describe Routes::Graph do
  let(:map_name) { 'SP' }
  let!(:map) { create(:map, name: map_name) }

  let!(:a_b) { create(:route, source: 'A', destination: 'B', map_id: map.id, distance: 10) }
  let!(:b_d) { create(:route, source: 'B', destination: 'D', map_id: map.id, distance: 15) }
  let!(:a_c) { create(:route, source: 'A', destination: 'C', map_id: map.id, distance: 20) }
  let!(:c_d) { create(:route, source: 'C', destination: 'D', map_id: map.id, distance: 30) }
  let!(:b_e) { create(:route, source: 'B', destination: 'E', map_id: map.id, distance: 50) }
  let!(:d_e) { create(:route, source: 'D', destination: 'E', map_id: map.id, distance: 30) }

  let(:graph) { described_class.new(map_name) }

  describe '#nodes' do
    subject { graph.nodes.sort }

    it { is_expected.to eq ['A', 'B', 'C', 'D', 'E'] }
  end

  describe '#edges' do
    let(:edges) do
      {
        "A" => [["B", 10.0], ["C", 20.0]],
        "B" => [["D", 15.0], ["E", 50.0]],
        "C" => [["D", 30.0]],
        "D" => [["E", 30.0]]
      }
    end

    subject { graph.edges }

    it { is_expected.to eq edges }
  end
end
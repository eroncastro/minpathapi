describe Routes::OptimumPathFinder do
  describe '#find_optimum_path' do
    let(:map_name) { 'SP' }
    let!(:map) { create(:map, name: map_name) }

    let!(:a_b) { create(:route, source: 'A', destination: 'B', map_id: map.id, distance: 10) }
    let!(:b_d) { create(:route, source: 'B', destination: 'D', map_id: map.id, distance: 15) }
    let!(:a_c) { create(:route, source: 'A', destination: 'C', map_id: map.id, distance: 20) }
    let!(:c_d) { create(:route, source: 'C', destination: 'D', map_id: map.id, distance: 30) }
    let!(:b_e) { create(:route, source: 'B', destination: 'E', map_id: map.id, distance: 50) }
    let!(:d_e) { create(:route, source: 'D', destination: 'E', map_id: map.id, distance: 30) }

    let(:args) do
      {
        map_name: map_name,
        source: source,
        destination: destination,
        autonomy: autonomy,
        fuel_liter_price: fuel_liter_price
      }
    end

    let(:autonomy) { '10' }
    let(:fuel_liter_price) { '2.5' }

    subject(:optimum_path) { described_class.new(args).find_optimum_path }

    context 'when path starts at A and ends at B' do
      let(:source) { 'A' }
      let(:destination) { 'D' }

      it { expect(optimum_path[:nodes]).to eq 'A B D' }
      it { expect(optimum_path[:distance]).to eq 25.0 }
      it { expect(optimum_path[:cost]).to eq 6.25  }
    end
  end
end

FactoryGirl.define do
  sequence(:name) { |n| "City#{n}" }
  sequence(:distance) { |n| (1..100).to_a.shuffle[n] }

  factory(:map) do
    name
  end

  factory(:route) do
    map
    source { generate(:name) }
    destination { generate(:name) }
  end
end

describe V1::RoutesController do
  describe 'GET#index' do
    let(:map_name) { 'São Paulo' }
    let!(:map) { create(:map, name: map_name) }

    let!(:a_b) { create(:route, source: 'A', destination: 'B', map_id: map.id, distance: 10) }
    let!(:b_d) { create(:route, source: 'B', destination: 'D', map_id: map.id, distance: 15) }
    let!(:a_c) { create(:route, source: 'A', destination: 'C', map_id: map.id, distance: 20) }
    let!(:c_d) { create(:route, source: 'C', destination: 'D', map_id: map.id, distance: 30) }
    let!(:b_e) { create(:route, source: 'B', destination: 'E', map_id: map.id, distance: 50) }
    let!(:d_e) { create(:route, source: 'D', destination: 'E', map_id: map.id, distance: 30) }

    let(:params) do
      {
        map_name: 'São Paulo',
        source: 'A',
        destination: 'B',
        autonomy: '10',
        fuel_liter_price: '2.5'
      }
    end

    context 'when get is correctly performed' do
      before do
        request.headers['Content-Type'] = 'application/json'
        get :index, params, format: :json
      end

      it { expect(response).to have_http_status(200) }
    end
  end

  describe 'POST#create' do
    let(:params) do
      {
        name: name,
        routes: routes
      }
    end
    let(:name) { 'São Paulo' }
    let(:routes) do
      [
        ["A", "B", 10],
        ["B", "D", 15],
        ["A", "C", 20],
        ["C", "D", 30],
        ["B", "E", 50],
        ["D", "E", 30]
      ]
    end

    context 'when Content-Type is invalid' do
      before do
        request.headers['Content-Type'] = 'invalid'
        post :create, params
      end

      it { expect(response).to have_http_status(415) }
    end

    [:name, :routes].each do |param|
      context "when #{param} is not present" do
        before do
          params.delete(param)
          request.headers['Content-Type'] = 'application/json'
          post :create, params
        end

        it { expect(response).to have_http_status(400) }
      end
    end

    context 'when params are valid' do
      before do
        request.headers['Content-Type'] = 'application/json'
        post :create, params
      end

      it { expect(response).to have_http_status(201) }
    end
  end
end

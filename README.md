# Minpathapi

## Início
Para rodar a aplicação, o primeiro passo é executar o processo de criação de migração do banco de dados. Comandos:

    bundle exec rake db:create db:migrate

## Rodando testes

    RAILS_ENV=test bundle exec rake db:create db:migrate
    bundle exec rspec

## Salvando rotas

Dado um mapa com nome e rotas, a ideia básica desta aplicação é salvar registros em um banco Postgres para poder pesquisá-los posteriormente. Para isso, suba a aplicação com o comando:

    bundle exec rails server

Em seguida, de posse de um RESTclient, faça uma requisição POST para a URL

    http://localhost:3000/v1/routes
- Com Content-Type
    'application/json'

- Com o body

    {
        "name": "SP",
        "routes": [
            ["A", "B", 10],
            ["B", "D", 15],
            ["A", "C", 20],
            ["C", "D", 30],
            ["B", "E", 50],
            ["D", "E", 30]
        ]
    }

## Melhor rota

    Para determinar a melhor rota, faça um GET no seguinte formato:

    http://localhost:3000/v1/routes?map_name=SP&source=A&destination=E&autonomy=10&fuel_liter_price=2.5